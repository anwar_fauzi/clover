import 'package:feda_clover/pages/page_otp_code.dart';
import 'package:feda_clover/pages/page_login.dart';
import 'package:feda_clover/pages/page_main.dart';
import 'package:feda_clover/pages/page_register.dart';
import 'package:feda_clover/pages/page_splash_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => const SplashScreenPage(),
        '/main': (context) => const MainPage(),
        '/login': (context) => const LoginPage(),
        '/register': (context) => const RegisterPage(),        
        '/otp': (context) => const OTPCodePage(),
      },
    );
  }
}
