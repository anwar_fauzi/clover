import 'package:feda_clover/shared/bottom_navbar.dart';
import 'package:feda_clover/shared/theme.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  /// Class MainPage akan menampilkan halaman utama 
  /// 
  /// halaman ini akan menampilkan product product yang akan dijual
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget heading() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            height: 70,
            decoration: BoxDecoration(
              color: grey3Color,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const SizedBox(),
                Row(
                  children: [
                    Container(
                      width: 222,
                      height: 40,
                      decoration: BoxDecoration(
                        color: grey3Color,
                        borderRadius: BorderRadius.circular(24),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 18,
                            height: 18,
                            margin: const EdgeInsets.only(left: 12),
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('assets/icon_search.png'),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 40,
                              margin: const EdgeInsets.only(left: 10, top: 10),
                              child: TextFormField(
                                decoration: InputDecoration.collapsed(
                                  hintText: 'Belanja apa hari ini ?',
                                  hintStyle:
                                      grey1TextStyle.copyWith(fontSize: 16),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const Spacer(),
                    Container(
                      width: 30,
                      height: 30,
                      margin: const EdgeInsets.only(right: 30),
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/icon_cart.png'),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox()
              ],
            ),
          ),
        ],
      );
    }

    Widget promo() {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            Container(
              width: 405,
              height: 175,
              margin: const EdgeInsets.only(top: 56),
              decoration: BoxDecoration(
                color: grey3Color,
                image: const DecorationImage(
                  image: AssetImage('assets/background.png'),
                ),
              ),
              child: Center(
                child: Text(
                  'Slider Pertama',
                  style: green1TextStyle.copyWith(
                      fontSize: 24, fontWeight: medium),
                ),
              ),
            ),
            Container(
              width: 405,
              height: 175,
              margin: const EdgeInsets.only(top: 56),
              decoration: BoxDecoration(
                color: green1Color,
                image: const DecorationImage(
                  image: AssetImage('assets/background.png'),
                ),
              ),
              child: Center(
                child: Text(
                  'Slider Kedua',
                  style: green1TextStyle.copyWith(
                      fontSize: 24, fontWeight: medium),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget contentProduct1() {
      return Container(
        width: 150,
        height: 300,
        margin: EdgeInsets.only(top: defaultMargin),
        decoration: BoxDecoration(
            color: white1Color,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: blackColor.withOpacity(0.2),
                  blurRadius: 5.0,
                  spreadRadius: 1,
                  offset: const Offset(0, 9))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 150,
              height: 120,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                image: DecorationImage(
                  image: AssetImage('assets/image1.png'),
                ),
              ),
            ),
            Container(
              width: 103,
              height: 23.5,
              margin: const EdgeInsets.only(top: 7),
              child: Text(
                'Kangkung Bandung, kualitas prima per ikat',
                style: blackTextStyle.copyWith(fontSize: 10),
              ),
            ),
            const SizedBox(height: 13),
            Text(
              'Rp. 5000',
              style: blackTextStyle.copyWith(fontWeight: bold),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Text(
                  'Rp.8000',
                  style: grey1TextStyle.copyWith(
                      fontSize: 10, decoration: TextDecoration.lineThrough),
                ),
                Container(
                  width: 20,
                  height: 10,
                  margin: const EdgeInsets.only(left: 6),
                  decoration: BoxDecoration(
                    color: green1Color,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Center(
                    child: Text(
                      '30%',
                      style: green1TextStyle.copyWith(
                          fontSize: 8, fontWeight: bold),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Text.rich(
              TextSpan(
                  text: 'Bandung, ',
                  style: blackTextStyle.copyWith(fontSize: 8),
                  children: [
                    TextSpan(
                      text: 'Jawa Barat',
                      style: blackTextStyle.copyWith(fontSize: 8),
                    )
                  ]),
            )
          ],
        ),
      );
    }

    Widget contentProduct2() {
      return Container(
        width: 150,
        height: 200,
        margin: EdgeInsets.only(top: defaultMargin, left: 9),
        decoration: BoxDecoration(
            color: white1Color,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: blackColor.withOpacity(0.5),
                  blurRadius: 5.0,
                  spreadRadius: 5,
                  offset: const Offset(0, 9))
            ]),
        child: Column(
          children: [
            Container(
              width: 150,
              height: 84,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                image: DecorationImage(
                  image: AssetImage('assets/image1.png'),
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget daftarProduct() {
      return Container(
        margin: EdgeInsets.all(defaultMargin),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Daftar Produk',
              style: grey1TextStyle.copyWith(fontSize: 22, fontWeight: bold),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Column(
                children: [
                  Row(
                    children: [
                      contentProduct1(),
                      contentProduct2(),
                    ],
                  ),
                  Row(
                    children: [
                      contentProduct1(),
                      contentProduct2(),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Container(
        decoration: BoxDecoration(color: white1Color),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [promo(), daftarProduct()],
        ),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Stack(
            children: [
              const Align(
              alignment: Alignment.center,
                child: BottomNavbar(),
              ),
              content(),
            ],
          ),
        ),
      ),
    );
  }
}
